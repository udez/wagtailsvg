# Generated by Django 3.1.11 on 2021-05-28 09:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailsvg', '0002_svg_edit_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='svg',
            name='file',
            field=models.FileField(upload_to='', verbose_name='file'),
        ),
    ]
